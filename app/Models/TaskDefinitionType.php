<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskDefinitionType extends Model
{
    use CrudTrait;
    use HasFactory;
    const DEFAULT_TYPE = 1;
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
    public function task_definitions(){
        return $this->belongsTo('App\Models\TaskDefinition', 'task_definition_type_id');
    }
}
