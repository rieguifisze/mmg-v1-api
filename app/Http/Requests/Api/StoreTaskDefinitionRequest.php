<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskDefinitionRequest;


class StoreTaskDefinitionRequest extends TaskDefinitionRequest
{
    use ApiRequestTrait;
}
