<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('plant-family', 'PlantFamilyCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('zone', 'ZoneCrudController');
    Route::crud('task-definition-type', 'TaskDefinitionTypeCrudController');
    Route::crud('plant-definition', 'PlantDefinitionCrudController');
    Route::crud('plant', 'PlantCrudController');
    Route::crud('task-definition', 'TaskDefinitionCrudController');
    Route::crud('task', 'TaskCrudController');
}); // this should be the absolute last line of this file